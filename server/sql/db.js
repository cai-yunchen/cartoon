const mysql = require("mysql");

const base = (sql, data) => {
  return new Promise((resolve, reject) => {
    const connextion = mysql.createConnection({
      host: "localhost",
      user: "root",
      port: 3306,
      database: "cartoon",
      password: "root",
    });
    connextion.connect();

    connextion.query(sql, data, (err, results, fields) => {
      if (err) return reject(err);
      resolve(results);
    });
    // 
    connextion.end();
  });
};
module.exports.query = base;