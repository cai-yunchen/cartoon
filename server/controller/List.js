const db = require('../sql/db');

class List {
  // 展示所有 list
  async ShowList(req, res) {
    try {
      const sql = `SELECT * FROM list`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '展示失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }

  }

  // 展示3条
  async ShowPageList(req, res) {
    const { i } = req.query
    try {
      const sql = `SELECT * FROM list LIMIT ${i}, 3;`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '展示失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

  // 按照 type 展示
  async ShowTypeList(req, res) {
    const { type } = req.query
    try {
      const sql = `SELECT * FROM list WHERE type LIKE '%${type}%';`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '展示失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

  // 按照 id 展示
  async ShowIdList(req, res) {
    const { id } = req.query
    try {
      const sql = `SELECT * FROM list WHERE id = ${id};`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '展示失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

}



module.exports = new List()