const db = require('../sql/db');

class Account {
  // 注册
  async Register(req, res) {
    const { username, pwd } = req.body
    const sql = `SELECT * FROM user WHERE username = '${username}'`
    const result = await db.query(sql)
    try {
      if (result.length > 0) {
        res.send({
          code: 404,
          msg: '用户名已注册!'
        })
      } else {
        const sql = `INSERT INTO user (username,pwd) VALUES (?,?)`
        const data = [username, pwd]
        const result = await db.query(sql, data)
        if (result.affectedRows > 0) {
          res.send({
            code: 200,
            msg: '注册成功！'
          })
        } else {
          res.send({
            code: 401,
            msg: '注册失败！'
          })
        }
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }

  }

  // 登入
  async Login(req, res) {
    const {
      username,
      pwd
    } = req.body
    const sql = `SELECT * FROM user WHERE username = '${username}'`
    const result = await db.query(sql)
    try {
      if (result.length > 0) {
        const sql = `SELECT * FROM user WHERE username = ? AND pwd = ?`
        const data = [username, pwd]
        const result = await db.query(sql, data)
        if (result.length > 0) {
          res.send({
            code: 200,
            msg: '登录成功！'
          })
        } else {
          res.send({
            code: 402,
            msg: '密码有误！'
          })
        }
      } else {
        res.send({
          code: 401,
          msg: '用户名有误！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })

    }
  }

}

module.exports = new Account()