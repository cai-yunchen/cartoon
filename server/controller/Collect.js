const db = require('../sql/db');

class Collect {
  // 添加收藏（有就添加，没有就删除）
  async addCollect(req, res) {
    const { username, id_s, img, title, cp, hot, introduce, start, end } = req.body
    try {
      const sql = `SELECT * FROM collect WHERE username = '${username}' AND id_s = '${id_s}'`
      const result = await db.query(sql)
      if (result.length > 0) {
        const sql = `DELETE FROM collect WHERE username = '${username}' AND id_s = '${id_s}'`
        const result = await db.query(sql)
        if (result.affectedRows == 1) {
          res.send({
            code: 201,
            msg: '取消收藏！'
          })
        } else {
          res.send({
            code: 401,
            msg: '取消收藏失败！'
          })
        }
      } else {
        const sql = `INSERT INTO collect (username,id_s,img,title,cp,hot,introduce,start,end)VALUES(?,?,?,?,?,?,?,?,?)`
        const data = [username, id_s, img, title, cp, hot, introduce, start, end]
        const result = await db.query(sql, data)
        if (result.affectedRows == 1) {
          res.send({
            code: 200,
            msg: '收藏成功！'
          })
        } else {
          res.send({
            code: 402,
            msg: '收藏失败！'
          })
        }
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

  // 展示所有收藏
  async showCollect(req, res) {
    const {
      username
    } = req.query
    try {
      const sql = `SELECT * FROM collect WHERE username = '${username}'`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '数据有误！'
        })
      }

    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

  // 展示 3条 收藏
  async showPageCollect(req, res) {
    const { username, i } = req.query
    try {
      const sql = `SELECT * FROM collect WHERE username = '${username}' LIMIT ${i}, 3;`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '数据有误！'
        })
      }

    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

  // 删除收藏
  async deleteCollect(req, res) {
    const {
      id
    } = req.body
    try {
      const sql = `DELETE FROM collect WHERE id = ${id}`
      const result = await db.query(sql)
      if (result.affectedRows == 1) {
        res.send({
          code: 200,
          msg: '取消收藏！'
        })
      } else {
        res.send({
          code: 401,
          msg: '取消收藏失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }
  }

}

module.exports = new Collect()