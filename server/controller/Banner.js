const db = require('../sql/db');

class Banner {
  // 展示轮播图
  async ShowBanner(req, res) {
    try {
      const sql = `SELECT * FROM banner`
      const result = await db.query(sql)
      if (result.length > 0) {
        res.send({
          code: 200,
          data: result
        })
      } else {
        res.send({
          code: 401,
          msg: '展示失败！'
        })
      }
    } catch (error) {
      res.status(500).send({
        code: 500,
        msg: '服务器内容错误！',
        data: error
      })
    }

  }
}

module.exports = new Banner()