const express = require('express')
const cors = require('cors');
const path = require('path');
const app = express()
const port = 3000

const AccountRouter = require('./routers/AccountRouter')
const BannerRouter = require('./routers/BannerRouter')
const ListRouter = require('./routers/ListRouter')
const CollectRouter = require('./routers/CollectRouter')

// 跨域
app.use(cors())
// 托管静态资源
app.use("/public", express.static(path.join(__dirname, 'public')))
// 允许传参
app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))

app.use('/api', AccountRouter)
app.use('/api', BannerRouter)
app.use('/api', ListRouter)
app.use('/api', CollectRouter)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))