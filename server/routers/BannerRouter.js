const router = require('express').Router();

const Banner = require('../controller/Banner');

router.get('/showBanner', Banner.ShowBanner)

module.exports = router