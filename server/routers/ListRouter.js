const router = require('express').Router();

const List = require('../controller/List');

router.get('/showList', List.ShowList)
router.get('/showPageList', List.ShowPageList)
router.get('/showTypeList', List.ShowTypeList)
router.get('/showIdList', List.ShowIdList)

module.exports = router