const router = require('express').Router();

const Collect = require('../controller/Collect');

router.post('/addCollect', Collect.addCollect)
router.get('/showCollect', Collect.showCollect)
router.get('/showPageCollect', Collect.showPageCollect)
router.post('/deleteCollect', Collect.deleteCollect)

module.exports = router