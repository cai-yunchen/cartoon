const router = require('express').Router();


const Account = require('../controller/Account');

router.post('/register', Account.Register)
router.post('/login', Account.Login)


module.exports = router