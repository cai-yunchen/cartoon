/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : cartoon

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2024-03-25 00:47:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('1', 'imgs/slideshow_01.png');
INSERT INTO `banner` VALUES ('2', 'imgs/slideshow_02.png');
INSERT INTO `banner` VALUES ('3', 'imgs/slideshow_03.png');
INSERT INTO `banner` VALUES ('4', 'imgs/slideshow_04.png');

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `hot` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `start` int(20) DEFAULT NULL,
  `end` int(100) DEFAULT NULL,
  `id_s` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES ('19', '菜菜一米八', '第二模式', '超能力App 看透伪装的人心', '278万', '第二模式”app，是一款能让人拥有超能力的软件。性格懦弱的高中生叶秋无意之中收到了试玩邀请，获得了听见心声的能力，而他的妹妹叶紫也因为被变态杀人狂袭击而受了重伤。叶秋决定用”第二模式”的能力查出真凶，可他还不知道，获得能力的人，不止他一个......【独家/已完结 责编: 哲亚】', '/imgs/list_05.png', '1', '36', '5');
INSERT INTO `collect` VALUES ('20', '菜菜一米八', '美人老矣', '王府公子x毒医妖女', '215万', '天下第一医师徐长灯坠崖身亡，其女徐悠然前去投靠瑞王爷不想路上遭侍女毒手并失忆，而侍女青露则趁机冒充前往瑞王府想要成为世子妃。让人没想到的是，如今在悠然身体里的是曾震慑江湖的毒医妖女，乐千秋的灵魂。毒医重生，曾经掀起血雨腥风的她，该如何度过这一世呢?【独家/已完结】', '/imgs/list_03.png', '1', '70', '3');
INSERT INTO `collect` VALUES ('21', '菜菜一米八', '23秒外', '身处虚幻和假象你会怎么做？', '88151', '当你感觉身处虚幻和假象你会怎么做? 少年从高桥上一跃而下，感受到了另一个世界遥远的呼唤，少年究竟想要去往何方? 如果23秒是钥匙，那我必将研砺前行! 哪怕等待我的前方是......金龙奖最佳剧情金奖作品!现实和虚幻之间奇妙的交织与选择，突破世界的边界，寻找我想要的真相!【独家/完结 责编:莫莉】', '/imgs/list_06.png', '1', '26', '6');
INSERT INTO `collect` VALUES ('22', '菜菜一米八', '告白', '深情飞行员x温软医生', '92.5万', '痞帅深情飞行员周京泽x温软坚定医生许随。夏天永远热烈，我爱的少年也是。看”痴情浪了”周京泽演绎这一份情有独钟。【每月3、13、23日更新|责编: 小红】', '/imgs/list_01.png', '1', '40', '1');

-- ----------------------------
-- Table structure for list
-- ----------------------------
DROP TABLE IF EXISTS `list`;
CREATE TABLE `list` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `hot` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `start` int(20) DEFAULT NULL,
  `end` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of list
-- ----------------------------
INSERT INTO `list` VALUES ('1', '告白', '恋爱', '深情飞行员x温软医生', '92.5万', '痞帅深情飞行员周京泽x温软坚定医生许随。夏天永远热烈，我爱的少年也是。看”痴情浪了”周京泽演绎这一份情有独钟。【每月3、13、23日更新|责编: 小红】', '/imgs/list_01.png', '1', '40');
INSERT INTO `list` VALUES ('2', '朝花惜时', '恋爱', '双面男主x学霸少女', '1453.3万', '他，戴上眼镜是沉默冷峻的高中生，摘下眼镜是浪荡不羁的夜店王子。女主同时遇上他们，想不到竟是同一人? 画风超绝美，男主帅到令人室息!引爆你的少女心!【已完结】', '/imgs/list_02.png', '1', '9');
INSERT INTO `list` VALUES ('3', '美人老矣', '古风', '王府公子x毒医妖女', '215万', '天下第一医师徐长灯坠崖身亡，其女徐悠然前去投靠瑞王爷不想路上遭侍女毒手并失忆，而侍女青露则趁机冒充前往瑞王府想要成为世子妃。让人没想到的是，如今在悠然身体里的是曾震慑江湖的毒医妖女，乐千秋的灵魂。毒医重生，曾经掀起血雨腥风的她，该如何度过这一世呢?【独家/已完结】', '/imgs/list_03.png', '1', '70');
INSERT INTO `list` VALUES ('4', '浮生物语', '古风', '龙子x树妖', '233万', '千年树妖袭楞独自活到现世，成了”不停”甜品店的老板娘。她的店里会来很多奇怪的客人，每个客人都会喝她的浮生茶，给她说一个自己的故事，那是妖怪们荡气回肠的爱恨情仇......【授权/完结 责编: Echo】', '/imgs/list_04.png', '1', '45');
INSERT INTO `list` VALUES ('5', '第二模式', '悬疑', '超能力App 看透伪装的人心', '278万', '第二模式”app，是一款能让人拥有超能力的软件。性格懦弱的高中生叶秋无意之中收到了试玩邀请，获得了听见心声的能力，而他的妹妹叶紫也因为被变态杀人狂袭击而受了重伤。叶秋决定用”第二模式”的能力查出真凶，可他还不知道，获得能力的人，不止他一个......【独家/已完结 责编: 哲亚】', '/imgs/list_05.png', '1', '36');
INSERT INTO `list` VALUES ('6', '23秒外', '悬疑', '身处虚幻和假象你会怎么做？', '88151', '当你感觉身处虚幻和假象你会怎么做? 少年从高桥上一跃而下，感受到了另一个世界遥远的呼唤，少年究竟想要去往何方? 如果23秒是钥匙，那我必将研砺前行! 哪怕等待我的前方是......金龙奖最佳剧情金奖作品!现实和虚幻之间奇妙的交织与选择，突破世界的边界，寻找我想要的真相!【独家/完结 责编:莫莉】', '/imgs/list_06.png', '1', '26');
