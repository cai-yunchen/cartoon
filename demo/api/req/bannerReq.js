import request from '../../utils/request'
import Banner from '../url/bannerUrl'

class bannerReq {
  ShowBanner(){
    return request(Banner.showBanner)
  }
}

export default new bannerReq()