import request from '../../utils/request'
import Collect from '../url/collectUrl'

class collectReq {
  AddCollect(params){
    return request(Collect.addCollect, params, 'POST')
  }
  ShowCollect(params){
    return request(Collect.showCollect, params)
  }
  ShowPageCollect(params){
    return request(Collect.showPageCollect, params)
  }
  DeleteCollect(params){
    return request(Collect.deleteCollect, params)
  }
}

export default new collectReq()