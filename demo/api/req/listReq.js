import request from '../../utils/request'
import List from '../url/listUrl'

class listReq {
  ShowList(){
    return request(List.showList)
  }
  ShowPageList(params){
    return request(List.showPageList, params)
  }
  ShowTypeList(params){
    return request(List.showTypeList, params)
  }
  ShowIdList(params){
    return request(List.showIdList, params)
  }
}

export default new listReq()