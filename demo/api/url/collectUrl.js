class Collect {
  static addCollect = '/addCollect'
  static showCollect = '/showCollect'
  static showPageCollect = '/showPageCollect'
  static deleteCollect = '/deleteCollect'
}

export default Collect