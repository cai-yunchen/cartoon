class List {
  static showList = '/showList'
  static showPageList = '/showPageList'
  static showTypeList = '/showTypeList'
  static showIdList = '/showIdList'
}

export default List