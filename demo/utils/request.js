import config from "../config/config";
export default (url, data = {}, method = 'GET') => {
  return new Promise((resolve, reject) => {
    // 1. new Promise初始化promise实例的状态为pending
    wx.request({
      url: config.host + url,
      data,
      method,
      success: (res) => {
        // console.log(res);
        resolve(res.data);
      },
      fail: (err) => {
        console.log(err);
        reject(err)
      }
    })
  })
}