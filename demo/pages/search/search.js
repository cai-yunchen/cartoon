// pages/search/search.js
import listReq from '../../api/req/listReq'

Page({
  /* 页面的初始数据 */
  data: {
    list: [],
    dataFlag: false,
    type: '',
  },

  // 跳转详情
  GoTo(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/details/details?id=${id}`,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    // console.log(options);
    try {
      const result = await listReq.ShowList()
      const listAll = result.data
      let flag = false
      listAll.forEach(item => {
        if (item.type == options.type) {
          flag = true
          return
        }
      })
      if (flag) {
        this.setData({
          type: options.type
        })
        const result1 = await listReq.ShowTypeList(options)
        if (result1.code == 200) {
          this.setData({
            list: result1.data,
            dataFlag: false
          })
        } else {
          this.setData({
            list: result1.data,
            dataFlag: true
          })
        }
      } else {
        this.setData({
          dataFlag: true,
          type: ''
        })
      }
    } catch (error) {
      console.log(error);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})