// pages/index/index.js
import listReq from '../../api/req/listReq'
import collectReq from '../../api/req/collectReq'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },

  // 展示列表
  async GetList(i) {
    const arr = []
    arr.push(...this.data.list)
    try {
      const result = await listReq.ShowPageList({
        i: i
      })
      const result1 = await collectReq.ShowCollect({
        username: "菜菜一米八"
      })
      if (result.code == 200) {
        arr.push(...result.data)
        if (result1.code == 200) {
          const newArray = arr.map(item => ({
            ...item,
            msg: result1.data.some(obj => obj.id_s === item.id) ? '已关注' : '+关注'
          }));
          this.setData({
            list: newArray
          })
        } else {
          arr.forEach(item => {
            item.msg = '+关注'
          })
          this.setData({
            list: arr
          })
        }
      }
    } catch (error) {
      console.log(error);
    }
  },

  GetMsg(data){
    const list = this.data.list
    list.forEach(item =>{
      if (item.id == data.detail.id) {
        item.msg = data.detail.msg
      }
    })
    this.setData({
      list:list
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.GetList(0)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    setTimeout(() => {
      this.GetList(this.data.list[this.data.list.length - 1].id)
    }, 500);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})