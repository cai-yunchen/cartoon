// pages/book/book.js
import listReq from '../../api/req/listReq'
import collectReq from '../../api/req/collectReq'

Page({
  /* 页面的初始数据 */
  data: {
    title:'猜你喜欢',
    list:[],
    listAll:[],
    collect:[]
  },

  async GetList(title,i){
    this.setData({
      list: [],
      listAll: []
    })
    try {
      let result1 = ''
      let result = ''
      if (title == '猜你喜欢') {
        result1 = await listReq.ShowList()
        result = await listReq.ShowPageList({i:i})
      } else if(title == '收藏') {
        result1 = await collectReq.ShowCollect({username:'菜菜一米八'})
        result = await collectReq.ShowPageCollect({username:'菜菜一米八', i:i})
      }
      if (result.code == 200) {
        this.setData({
          list: result.data,
          listAll: result1.data
        })
      }
    } catch (error) {
      console.log(error);
    }
  },

  async GetSon(data){
     this.setData({
      title:data.detail.type
    })
    this.GetList(data.detail.type,0)
  },

  /* 生命周期函数--监听页面加载 */
   onLoad(options) {
     this.GetList(this.data.title,0)
  },

  /* 页面相关事件处理函数--监听用户下拉动作 */
  onPullDownRefresh() {
    const list = this.data.list
    const listAll = this.data.listAll
    if (listAll[0].id_s) {
      if (list[list.length - 1].id == listAll[listAll.length - 1].id) {
        this.GetList(this.data.title,0)
      } else {
        this.GetList(this.data.title,3)
      }
    } else{
      if (list[list.length - 1].id == listAll[listAll.length - 1].id) {
        this.GetList(this.data.title,0)
      } else {
        this.GetList(this.data.title,list[list.length - 1].id)
      }
    }

    wx.stopPullDownRefresh()
  },
})