// pages/details/details.js
import listReq from '../../api/req/listReq'
import collectReq from '../../api/req/collectReq'

Page({
  /**
   * 页面的初始数据
   */
  data: {
    item: {
      img:'/imgs/list_01.png'
    },
    msg:''
  },

  // 添加收藏
  async Attention() {
    const item = this.data.item
    const username = '菜菜一米八'
    const parmas = {
      ...item,
      username,
      id_s: item.id
    }
    try {
      const result = await collectReq.AddCollect(parmas)
      if (result.code == 200) {
        wx.showToast({
          title: '关注成功！'
        })
        this.setData({
          msg:'已关注'
        })
      } else {
        wx.showToast({
          title: '取消关注！',
          icon: 'error'
        })
        this.setData({
          msg:'+关注'
        })
      }
    } catch (error) {
      console.log(error);
    }
  },

  /* 生命周期函数--监听页面加载 */
  async onLoad(options) {
    try {
      const result = await listReq.ShowIdList(options)
      if (result.code == 200) {
        this.setData({
          item: result.data[0]
        })
        const result1 = await collectReq.ShowCollect({username:'菜菜一米八'})
        if (result1.code == 200) {
          let flag = false
          result1.data.forEach(item =>{
            if (item.id_s == result.data[0].id) {
              flag = true
              return
            }
          })
          if (flag) {
            this.setData({
              msg: '已关注'
            })
          } else {
            this.setData({
              msg: '+关注'
            })
          }
        } else {
          this.setData({
            msg: '+关注'
          })
        }
      } else {
        wx.showToast({
          title: result.msg,
          icon: 'error'
        })
      }
    } catch (error) {
      console.log(error);
    }
  },

  /* 生命周期函数--监听页面初次渲染完成 */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})