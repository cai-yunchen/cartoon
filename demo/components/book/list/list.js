// components/book/list/list.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    list: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    activeTab: 0,
    tap: ['猜你喜欢', '收藏'],
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 处理标签页切换事件
    onTabChange(event) {
      this.setData({
        activeTab: event.detail.index
      });
      this.triggerEvent("event_Collect", {
        type: event.detail.title
      })
    },

    // 跳转详情
    GoTo(e) {
      const item = e.currentTarget.dataset.item;
      if (item.id_s) {
        wx.navigateTo({
          url: `/pages/details/details?id=${item.id_s}`,
        })
      } else {
        wx.navigateTo({
          url: `/pages/details/details?id=${item.id}`,
        })
      }
    },
  }
})