// components/classfly/search/search.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 双向绑定数据
    InputChange(e){
      // console.log(e.detail.value);
      this.setData({
        inputVal:e.detail.value
      })
    },

    // 跳转搜索页面
    GetInput(){
     if (this.data.inputVal) {
        wx.navigateTo({
          url: `/pages/search/search?type=${this.data.inputVal}`,
        })
      } else {
        wx.showToast({
          title: '输入不能为空！',
          icon:'error'
        })
      }
    }
  }
})