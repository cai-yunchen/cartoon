// components/classfly/list/list.js
import listReq from '../../../api/req/listReq'

Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    activeTab: 0,
    listAll: [],
    typeList: [],
    listType: [],
  },

  /* 组件的方法列表 */
  methods: {
    // 处理标签页切换事件
    onTabChange(event) {
      this.setData({
        activeTab: event.detail.index
      });
      // console.log(event.detail.title);
      this.GetShow(event.detail.title)
    },

    // 获取typeList
    async GetShow(type) {
      try {
        const result = await listReq.ShowList()
        if (result.code == 200) {
          this.setData({
            listAll: result.data
          })
          // 提取 type 值并去重
          const Types = [...new Set(this.data.listAll.map(item => item.type))];
          this.setData({
            typeList: Types
          })
          const result1 = await listReq.ShowTypeList({
            type: type
          })
          if (result1.code == 200) {
            this.setData({
              listType: result1.data
            })
          } else {
            wx.showToast({
              title: '暂无数据',
              icon: 'error'
            })
          }
        } else {
          wx.showToast({
            title: '暂无数据',
            icon: 'error'
          })
        }
      } catch (error) {
        console.log(error);
      }
    },

    // 跳转详情
    GoTo(e) {
      const id = e.currentTarget.dataset.id;
      wx.navigateTo({
        url: `/pages/details/details?id=${id}`,
      })
    },
  },
  // 生命周期
  lifetimes: {
    attached() {
      this.GetShow('恋爱')
    }
  },
})