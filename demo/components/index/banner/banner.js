// pages/components/home/top.js
import bannerReq from '../../../api/req/bannerReq'

Component({

    /**
     * 组件的属性列表
     */
    properties: {
  
    },
  
    /**
     * 组件的初始数据
     */
    data: {
      imgs: [],
      indicatorDots: true,
      vertical: false,
      autoplay: true,
      interval: 1500,
      duration: 500
    },
  
    /**
     * 组件的方法列表
     */
    methods: {
      
    },
    lifetimes:{
      async attached(){
        try {
          const result = await bannerReq.ShowBanner()
          if (result.code == 200) {
            this.setData({
              imgs: result.data
            })
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
  })