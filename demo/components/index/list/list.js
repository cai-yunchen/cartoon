import collectReq from '../../../api/req/collectReq'

Component({
  /* 组件的属性列表 */
  properties: {
    list:Object
  },

  /* 组件的初始数据 */
  data: {
    flagB:false,
    msg:'',
    icon:'',
    msg1:'+关注',
  },

  /* 组件的方法列表 */
  methods: {
    // 跳转详情
    GoTo(e){
      const id = e.currentTarget.dataset.id;
      wx.navigateTo({
        url: `/pages/details/details?id=${id}`,
      })
    },
    // 添加收藏
    async Attention(e){
      const item = e.currentTarget.dataset.item;
      const username = '菜菜一米八'
      const parmas = {...item , username, id_s:item.id}
      try {
        const result = await collectReq.AddCollect(parmas)
        if (result.code == 200) {
          wx.showToast({
            title: '关注成功！'
          })
          this.triggerEvent("event_Msg",{msg:"已关注",id:item.id})
        } else {
          wx.showToast({
            title: '取消关注！',
            icon:'error'
          })
          this.triggerEvent("event_Msg",{msg:"+关注",id:item.id})
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
})